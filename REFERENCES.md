# Книги из курса  
  
> Всё в каталоге "Country Reports"  
Comparative Politics Today: A World View, 11th edition  
G. Bingham Powell, Jr., Russell J. Dalton, Kaare W. Strøm  
UK: Pearson, 2015.  
  
> Smorgunov_chXX.pdf  
Сморгунов Л. В.  
Сравнительная политология: Учебник для вузов.  
СПб.: Питер, 2012.  
  
> golosov_chXX.pdf  
Сравнительная политология: Учебник, 3-е изд  
Голосов Г. В.  
СПб: издательство ЕУСПб, 2001  
  
> Landman_XXX.pdf  
Issues and Methods in Comparative Politics: an Introduction, 3rd ed  
Todd Landman  
London/NY: Routledge, 2008.  
  
> Grigsby_ch2.pdf  
Analyzing Politics: An Introduction to Political Science, 4th ed  
ELLEN GRIGSBY  
CA: WADSWORTH Cengage Learning, 2009  
  
> Caporaso-p1ch5.pdf  
The SAGE Handbook of Comparative Politics  
Edited by Todd Landman and Neil Robinson  
CA: SAGE, 2009  
  
> FoCP  
Foundations of Comparative Politics: Democracies of the Modern World, 2nd ed  
Kenneth Newton, Jan W. Van Deth  
Cambridge: Cambridge University Press, 2010  
  
> OHCP  
The Oxford Handbook Of Comparative Politics  
Edited by CARLES BOIX and SUSAN C. STOKES  
Oxford: Oxford University Press, 2007  
  
> CSES  
The Comparative Study of Electoral Systems  
Series editors: Hans-Dieter Klingemann and Ian McAllister  
Oxford: Oxford University Press, 2009  
  
> Hague_chXX.pdf // Книга с красными заголовками и номером главы в шестиугольнике  
Comparative Government and Politics: An introduction, 10th ed.  
Rod Hague, Martin Harrop, John McCormick  
NY: Palgrave, 2016  
  
> Hague_comp_gov_chXX.pdf  
Comparative Government and Politics: An Introduction, 6th ed.  
Rod Hague and Martin Harrop  
NY: Palgrave, 2004  
  
> ONeil_ch6.pdf  
ESSENTIALS OF COMPARATIVE POLITICS, 3rd ed  
Patrick H. O’Neil  
NY: W.W. Norton & Co, 2010  
  
> Samuels_ch4p3.pdf  
Comparative Politics  
David J. Samuels  
NJ: Pearson, 2013  
  
> Sodaro_ch5.pdf  
Comparative Politics: A Global Introduction   
Michael J. Sodaro  
NY: McGraw-Hill, 2008  
  
> Clark_et_al_p3ch10.pdf  
Principles of Comparative Politics, 3rd ed.  
William Roberts Clark, Matt Golder, Sona N. Golder  
CA: SAGE, 2018  
