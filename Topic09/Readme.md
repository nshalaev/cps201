# Текущее задание

В этот раз материал состоит из большого числа отдельных небольших фрагментов.  
Обратите внимание на то, что читать нужно именно части, посвящённые военным режимам.  
Для удобства страницы с этими фрагментами идентифицированы "как в книге" и в скобках "как в PDF-файле".  
Если какая-то глава вам понравится, можете, конечно, прочитать её целиком — это только приветствуется.  

golosov_ch11.pdf — 326 (13), "Армия"  
Hague_ch5.pdf — 65 (10), "Military government"  
Hague_comp_gov_ch4.pdf — 58 (8), "Military Rule"  
ONeil_ch6.pdf — 158 (18), "Military Rule"  
Samuels_ch4p3.pdf — 106 (3), "Military Regimes"  
Sodaro_ch5.pdf — 132 (9), "The Military" и "The Military Politics: Turkey and Pakistan"  
Clark_et_al_p3ch10.pdf — Целиком  

