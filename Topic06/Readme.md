# Текущее задание

FoCP_p3ch9-2.pdf — из Foundations of Comparative Politics (обратите внимание: вторая часть главы!)  
Hague_ch14.pdf — глава о политической коммуникации (врезку про Венесуэлу можно опустить)  
Sabetti_OHCP.pdf — "Democracy And Civic Culture"  

# Предыдущее задание

## Обязательное чтение
golosov_ch4.pdf — глава 4, "Политическая культура и участие"  
Hague_ch12.pdf — глава 12 из 10 изд. Comparative Government and Politics (Hague, Harrop, McCormick), "Political Culture" (врезку на сс. 208-209 можно пропустить). Текст из учебника, так что не очень сложный.  

## Частично
FoCP_p3ch9-1.pdf — из Foundations of Comparative Politics. __Обязательно изучите врезку на сс. 173-174 (Controversy 9.1), эти вопросы мы рассмотрим на семинаре.__  
В остальном этот текст похож на Hague et al., так что можете считать его чтением по желанию.

