## Тексты на русском (на следующее занятие)

smorgunov_ch2.pdf  
golosov_ch2.pdf  

## Тексты на английском (через неделю)

### OHCP_p2_ch2.pdf

__Multicausality, Context-Conditionally, and Endogeneity__  
Robert J. Franzese, Jr. is Associate Professor of Political Science at the University of Michigan, Ann Arbor.

### OHCP_p2_ch3.pdf

__Historical Enquiry and Comparative Politics__  
James Mahoney is an Associate Professor of Political Science and Sociology at Northwestern University.  
Celso M. Villegas is a graduate student in the Department of Sociology, Brown University.

### OHCP_p2_ch4.pdf

__The Case Study: What it is and What it Does__  
John Gerring is Associate Professor, Department of Political Science, Boston University.

### OHCP_p2_ch5.pdf

__Field Research__  
Elisabeth Jean Wood is Professor of Political Science at Yale University and Research Professor at the Santa Fe Institute.

### OHCP_p2_ch6.pdf

__Is the Science of Comparative Politics Possible?__  
Adam Przeworski is Carroll and Milton Petrie Professor in the Department of Politics, New York University.

### OHCP_p2_ch7.pdf

__From Case Studies to Social Science: A Strategy for Political Research__  
Robert H. Bates is Eaton Professor of the Science of Government at Harvard University.

